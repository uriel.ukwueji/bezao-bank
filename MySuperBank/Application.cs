﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySuperBank
{
      
    public static class Application
    {


        public static void Run()
        {
            
            var menu = new StringBuilder();
            menu.Append("Hello, Welcome to Bezao Bank\n ");
            menu.AppendLine("What will you like us to do for you:\n ");

            Console.Title = "Bezao Bank";
            Console.WriteLine();
            Console.WriteLine(menu.ToString());
            Console.WriteLine($" Open an Account: Press 1\n Use ATM Services: Press 2");
            string input = Console.ReadLine();
            input = Helpers.RepeatInput(input, "Open an Account" , "Use ATM Services");


            if (input == "1")    
                OpenAccount();
                    
          AtmServices();    //else

            


        }


        public static void OpenAccount()
        {
            Console.WriteLine("What is your Full name: ");
            var name = Console.ReadLine();

            while (Helpers.IsBlank(name))
            {
                Console.WriteLine("Cannot Enter Empty Full Name");
                Console.Write("Enter Full Name: ");
                name = Console.ReadLine();
            }

            var bankAccount = new BankAccount(name);
            Console.WriteLine();
            Console.WriteLine();
            AccountService.Register(bankAccount);
            Console.WriteLine();


            Console.WriteLine($"\nDo you wish to use ATM Services now\n YES: Press 1\n NO Press 2");
            var input = Console.ReadLine();

            input = Helpers.RepeatInput(input, "YES", "NO");

            if (input == "1")
                AtmServices();
            Console.WriteLine("Thanks for Banking with us, Have a nice Day");            

        }

        public static void AtmServices()
        {
            var AtmMachine = new Atm();



            Console.WriteLine("Welcome to Bezao ATM\n");
            Console.WriteLine("Choose a preferred Language:\n Press 1 for English\n Press 2 for Igbo");
            var language = Console.ReadLine();

           language = Helpers.RepeatInput(language, "English", "Igbo");

           if( AtmMachine.ChangeLanguage(language) == "English")
            {
                
                Console.WriteLine($"Type in your Account Number");
                var account = Console.ReadLine();
                Console.WriteLine($"Type in your pin: \n");
                var pin = Console.ReadLine();

                while (!AtmMachine.ConfirmUser(account, pin))
                {
                    Console.WriteLine($"Invalid User, type in a registered Account Number");
                     account = Console.ReadLine();

                    Console.WriteLine($"Type in your Pin:\n ");
                    pin = Console.ReadLine();
                }

                Console.WriteLine("what will you like to do");
                Console.WriteLine("Withdraw: Press 1\n Deposit: Press 2\n Check Balance: Press 3\n Change PIN: Press 4");
                var input = Console.ReadLine();

                input = Helpers.RepeatInput(input, "Withdraw", "Deposit", "Check Balance", "Change PIN");

                if(input == "1")
                {
                    Console.WriteLine("How much will you like to withdraw");
                    var amount = Convert.ToDecimal( Console.ReadLine());
                    
                    
                    while (!AtmMachine.ValidDenomination(amount))
                    {
                        Console.WriteLine("Amount must be multiples of 500 or 1000 ONLY");
                        amount = Convert.ToDecimal(Console.ReadLine());
                    }

                    AtmMachine.Withdraw(amount, DateTime.Now, $"Withdrew {amount} with Bezao Atm by {DateTime.Now.Hour}:{DateTime.Now.Second}");
                    Console.WriteLine($"\nDo you wish to perform another transaction\n YES: Press 1\n NO Press 2");
                    var input1 = Console.ReadLine();

                    input1 = Helpers.RepeatInput(input1, "YES", "NO");
                    if (input1 == "1")
                        AtmServices();
                    Console.WriteLine("Thanks for Banking with us, Have a nice Day");
                }

                else if(input == "2")
                {
                    Console.WriteLine("How much will you like to deposit");
                    var amount = Convert.ToDecimal(Console.ReadLine());

                    AtmMachine.Deposit(amount, DateTime.Now, $"Deposited {amount} with Bezao Atm by {DateTime.Now.Hour}:{DateTime.Now.Second}");

                    Console.WriteLine($"\nDo you wish to perform another transaction\n YES: Press 1\n NO Press 2");
                    var input1 = Console.ReadLine();

                    input1 = Helpers.RepeatInput(input1, "YES", "NO");
                    if (input1 == "1")
                          AtmServices();
                    Console.WriteLine("Thanks for Banking with us, Have a nice Day");
                }

                else if(input == "3")
                {
                    AtmMachine.CheckBalance("You Have Amount");
                    Console.WriteLine($"\nDo you wish to perform another transaction\n YES: Press 1\n NO Press 2");
                    var input1 = Console.ReadLine();

                    input1 = Helpers.RepeatInput(input1, "YES", "NO");

                    if (input1 == "1")

                        AtmServices();
                    Console.WriteLine("Thanks for Banking with us, Have a nice Day");
                }

                else if(input == "4")
                {

                    Console.WriteLine($"type your prev Pin");
                    var prevPin = Console.ReadLine();

                    Console.WriteLine($"type in your new Pin");
                    var newPin = Console.ReadLine();

                    AtmMachine.ChangePin(prevPin, newPin, "english");

                    Console.WriteLine($"\nDo you wish to perform another transaction\n YES: Press 1\n NO Press 2");
                    var input1 = Console.ReadLine();

                    input1 = Helpers.RepeatInput(input1, "YES", "NO");
                    if (input1 == "1")
                        AtmServices();
                    Console.WriteLine("Thanks for Banking with us, Have a nice Day4");
                }   

            }

            else
            {

                Console.WriteLine($"Pia Account Number gi");
                var account = Console.ReadLine();
                Console.WriteLine($"Pia Atm Pin gi: \n");
                var pin = Console.ReadLine();

                while (!AtmMachine.ConfirmUser(account, pin))
                {
                    Console.WriteLine($"Account Number a aburo Member anyi, Pia Account Number bu Member ebe a: \n");
                    account = Console.ReadLine();

                    Console.WriteLine($"Pia Pin gi:\n ");
                    pin = Console.ReadLine();
                }

                Console.WriteLine("Ke ihe ichoro ime");
                Console.WriteLine("Iwere Ego: Pia 1\n Itinye Ego: Pia 2\n Ifu Ego ewere: Pia 3\n Itughari Pin gi: Pia 4");
                var input = Console.ReadLine();

                input = Helpers.RepeatInput(input, "Iwere Ego:", "Itinye Ego", "Ifu Ego iwere", "Itughari Pin gi");

                if (input == "1")
                {
                    Console.WriteLine("Ego one ka ichoro iwere");
                    var amount = Convert.ToDecimal(Console.ReadLine());

                    while (!AtmMachine.ValidDenomination(amount))
                    {
                        Console.WriteLine("Soso 500 na 1000 ka anyi ji akwu ugwo");
                        amount = Convert.ToDecimal(Console.ReadLine());
                    }
                    AtmMachine.Withdraw(amount, DateTime.Now, $"iwere {amount} na Bezao Bank by {DateTime.Now.Hour}:{DateTime.Now.Second}");
                    Console.WriteLine($"\nE nwere ihe ozo ichoro ime\n Ehe Now: Pia 1\n Mba Pia 2");
                    var input1 = Console.ReadLine();

                    input1 = Helpers.RepeatInput(input, "Ehe Now", "Mba");

                    if (input1 == "1")

                        AtmServices();
                    Console.WriteLine("Daalu onye nke anyi, jee nke oma");
                }

                else if (input == "2")
                {
                    Console.WriteLine("Ego ole ka ichoro itinye: ");
                    var amount = Convert.ToDecimal(Console.ReadLine());

                    AtmMachine.Deposit(amount, DateTime.Now, $"Iji Bezao Bank Atm tinye {amount} na Account gi na {DateTime.Now.Hour}:{DateTime.Now.Second}");
                    
                    Console.WriteLine($"\nE nwere ihe ozo ichoro ime\n YES: Pia 1\n NO Pia 2");
                    var input1 = Console.ReadLine();
                    input1 = Helpers.RepeatInput(input, "YES", "NO");

                    if (input1 == "1")
                        AtmServices();
                    Console.WriteLine("Daalu onye nke anyi, jee nke oma");
                }

                else if(input == "3")
                {
                    AtmMachine.CheckBalance("Ego di na Account gi putara");
                    
                    Console.WriteLine($"\nE nwere ihe ozo ichoro ime\n YES: Pia 1\n NO Pia 2");
                    var input1 = Console.ReadLine();
                    input1 = Helpers.RepeatInput(input, "Ehe Now", "Mba");

                    if (input1 == "1")
                        AtmServices();
                    Console.WriteLine("Daalu onye nke anyi, jee nke oma");
                }

                else // change pin
                {
                    Console.WriteLine($"Pinye Pin gi ");
                    var prevPin = Console.ReadLine();

                    Console.WriteLine($"Pinye Pin Ohuru ichoro");
                    var newPin = Console.ReadLine();

                    AtmMachine.ChangePin(prevPin, newPin, "igbo");

                    Console.WriteLine($"\nE nwere ihe ozo ichoro ime\n YES: Pia 1\n NO Pia 2");
                    var input1 = Console.ReadLine();

                    input1 = Helpers.RepeatInput(input1, "Ehe Now", "Mba");
                    if (input1 == "1")
                        AtmServices();
                    Console.WriteLine("Daalu, jee Nke Oma, ya gazire gi...");
                }

            }

            
            

        }

    }
}
