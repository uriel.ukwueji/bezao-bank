﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySuperBank
{
    public class BankAccount
    {
        public string Number { get; }
        public string Owner { get; set; }

        private static int _accountNumberSeed = 1234567890;


        public BankAccount(string name)
        {

            Owner = name;
            Number =$"{ Owner[0]}{_accountNumberSeed.ToString()}";
            _accountNumberSeed++;
        }

    }
}
